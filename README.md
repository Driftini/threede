# Threede
## A not-an-IDE for Windows93
Threede is a Windows93 app which is objectively too incomplete to be a full-fledged IDE, though it has a few features which might be cool for some.
## Less time between typing and running
Threede's main feature is running JS without having to open the Terminal and paste the code in it.
Just press the **Run Code** button and you're good to go.
## Skip the repetitive parts and start coding your app
Templates are in the list of the planned features!
As of now, I can only confirm a template for DOM-based apps, but maybe I'll add in a CLI app template, most likely with help from some other members of the Windows93 community.
## Saving and loading
Nothing too special, no need for explanations.