let threede = $window({
    title: 'Threede',
    width: 400,
    height: 300,
    icon: 'https://gitlab.com/UmbraGIR/threede/raw/master/threede.png',
})

$threede = {
    runcode: function() {
        $exe("js " + codearea.value)
    },
    clear: function() {
        codearea.value = []
    }
}

let codearea = document.createElement("textarea")
codearea.id = "threede_codearea"
let run = document.createElement("button")
run.id = "threede_run"
run.innerText = "Run Code"
run.addEventListener("click", function(){$threede.runcode()})
let clr = document.createElement("button")
clr.id = "threede_clr"
clr.innerText = "Clear"
clr.addEventListener("click", function(){$threede.clear()})

threede.el.body.appendChild(codearea)
threede.el.body.appendChild(run)
threede.el.body.appendChild(clr)